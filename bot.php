<?php
include 'Config.php';
$a = fopen('DR.csv', "r");

while (($info = fgetcsv($a, 100, ',') ) !== false) {
    if (is_array($info)) {
        $name = $info[0];//имя
        $birthday = $info[1];//дата
    }


    $birthday = new DateTime($birthday);
    $today = new DateTime();
    $todayYear = $today->format('Y');
    $todayMonth = $birthday->format('m');
    $todayDay = $birthday->format('d');
    date_time_set($today, 0, 0,0,0);
    date_date_set($birthday, $todayYear, $todayMonth, $todayDay);

    if ($birthday > $today) {
        $days = $today->diff($birthday)->format('%a');
        if ($days == 1) {
            $mess = sprintf('Завтра день рождение у %s', $name);
            $tbot = file_get_contents("https://api.telegram.org/bot".$token."/sendMessage?chat_id=".$chatId."&text=".urlencode($mess));
        }
    }
}
